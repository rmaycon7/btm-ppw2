<note>
    <div class="modal fade" id="note">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
    
                <div class="modal-header">
                    <h4 class="modal-title">Notas do grupo</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
    
                <div class="modal-body">
                    <div class="form-group">
                        <div class="alert alert-success" role="alert" each={ note in this.state.dashboardReducer.group_data.notes }>
                            <div class="row text-left">
                                <h4 id="note_title" class="alert-heading">{ note.name }</h4>
                                <a onclick={edit.bind(this, note.name, note.content, note.id)}><img src="imgs/edit.svg" width="20"></a>
                            </div>
                            <p id="note_content">{ note.content }</p>
                            <hr>
                        </div>
                    </div>
                </div>
    
                <div class="modal-footer">
                    <button type="button" class="btn btn-success" onclick={add}>Adicionar Nota</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Voltar</button>
                </div>  
            </div>
        </div>
    </div>

    <script>
        this.mixin("state")
        this.mixin("reduxGlobal")
        this.mixin('subscribeStateTag')
        this.subscribeStateTag(this)

        edit(note_name, note_content, note_id){
            $('#note').modal('hide');
            $('#editNote').modal('show');

            this.dispatch({   
                type: 'SET_CURRENT_NOTE',
                payload: {
                    current_note_id: note_id,
                    current_note_name: note_name,
                    current_note_content: note_content
                }
            });
        }

        add(){
            
            $('#note').modal('hide');
            $('#addNote').modal('show');
        }
    </script>
</note>