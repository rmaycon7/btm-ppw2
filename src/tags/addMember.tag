<addMember>
    <div class="modal fade" id="addMember">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div id="spinner" if={this.state.exibicaoReducer.showSpinner}><img src="imgs/spinner.svg"></div>
                <div class="modal-header">
                    <h4 class="modal-title">Adicionar membro</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="emailMembro">Email:</label>
                        <input type="text" class="form-control" id="emailMembro" placeholder="Digite o email do novo membro">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-success" onclick={addMemberToGroup}>Enviar Solicitação</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Voltar</button>
                </div>

            </div>
        </div>
    </div>

    <script>
        this.mixin("state")
        this.mixin("reduxGlobal")
        this.mixin('subscribeStateTag')
        this.subscribeStateTag(this)
        
        addMemberToGroup(){
            this.dispatch({
                type: "ADD_MEMBER_TO_GROUP",
                payload: {
                    email: document.getElementById('emailMembro').value,
                    user_token: this.state.userReducer.user_data.user_token,
                    group_id: this.state.dashboardReducer.current_group_id
                }
            });
        }
        this.on('update', function() {
            if(this.state.userReducer.closeModal){
                $('#addMember').modal('hide');
            }
        })
    </script>

    <style>
        #spinner{
            position: absolute;
            width: 100%;
            z-index: 10;
            text-align: center;
            height: 100%;
            background: rgba(255, 255, 255, 0.7);
            padding-top: 7%;
        }
    </style>
</addMember>