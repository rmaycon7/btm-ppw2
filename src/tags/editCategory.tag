<editCategory>
    <div class="modal fade" id="editCategory">
        <div class="modal-dialog modal-dialog-centered">        
            <div class="modal-content">
                <div id="spinner" if={this.state.exibicaoReducer.showSpinner}><img src="imgs/spinner.svg"></div>
                <div class="modal-header">
                    <h4 class="modal-title">Editar Categoria</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="categoriaNome">Novo nome:</label>
                        <input type="text" class="form-control" id="categoriaNome" value="{opts.category_name}">
                    </div> 
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-success" onclick={editCategory}>Salvar</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Voltar</button>
                </div>
            </div>
        </div>
    </div>

    <script>
        this.mixin("state")
        this.mixin("reduxGlobal")
        this.mixin('subscribeStateTag')
        this.subscribeStateTag(this)

        editCategory(){
            let category_att = { id: opts.category_id, name: document.getElementById('categoriaNome').value, description: '' };
            let userinfo = { token: this.state.userReducer.user_data.user_token, id: this.state.userReducer.user_data.id };
            let group = { id: this.state.dashboardReducer.current_group_id };
            
            this.dispatch(
                { type: 'EDIT_CATEGORY',
                    payload: {
                        category_att,
                        userinfo,
                        group
                    }
                });

        }

        this.on('update', function() {
            if(this.state.userReducer.closeModal){
                $('#editCategory').modal('hide');
            }
        })
    </script>

    <style>
        #spinner{
            position: absolute;
            width: 100%;
            z-index: 10;
            text-align: center;
            height: 100%;
            background: rgba(255, 255, 255, 0.7);
            padding-top: 7%;
        }
    </style>

</editCategory>