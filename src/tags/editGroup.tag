<editGroup>
    <div class="modal fade" id="editGroup">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div id="spinner" if={this.state.exibicaoReducer.showSpinner}><img src="imgs/spinner.svg"></div>
                <div class="modal-header">
                    <h4 class="modal-title">Editar Grupo</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="nomeGroups">Nome do grupo:</label>
                        <input type="text" class="form-control" id="nomeGroup" value="{opts.group_name}" placeholder="{opts.group_name}" onchange={teste}>
                        <label for="descriptionGroup">Descrição do grupo:</label>
                        <textarea id="descriptionGroup" name="description" class="form-control" placeholder=""></textarea>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-success" onclick={editGroup}>Salvar</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal" onclick={deletarGrupo}>Excluir</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Voltar</button>
                </div>
            </div>
        </div>
    </div>

    <script>
        this.mixin("state")
        this.mixin("reduxGlobal")
        this.mixin('subscribeStateTag')
        this.subscribeStateTag(this)

        editGroup(){
            let groupAtt = { id: this.state.dashboardReducer.current_group_id, name: document.getElementById('nomeGroup').value };
            let userinfo = { token: this.state.userReducer.user_data.user_token, id: this.state.userReducer.user_data.id };

            this.dispatch(
                { type: 'EDIT_GROUP',
                    payload: {
                        groupAtt,
                        userinfo
                    }
            });
        }
        deletarGrupo(){
                let group = { id: this.state.dashboardReducer.current_group_id };
                let userinfo = { token: this.state.userReducer.user_data.user_token, id: this.state.userReducer.user_data.id };
                this.dispatch(
                    { type: 'DELETE_GROUP', 
                        payload: {
                            group,
                            userinfo
                        }
                });
        }

        this.on('update', function() {
            if(this.state.userReducer.closeModal){
                $('#editGroup').modal('hide');
            }
        })
    </script>

    <style>
        #spinner{
            position: absolute;
            width: 100%;
            z-index: 10;
            text-align: center;
            height: 100%;
            background: rgba(255, 255, 255, 0.7);
            padding-top: 7%;
        }
    </style>
</editGroup>