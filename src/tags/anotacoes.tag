<anotacoes>
    <style>    
        .titulo{
            text-align: left;
        }
        .texto{
            font-size: 20px;
        }
        .bhide{
           visibility: hidden; 
        }
        #relatorio1{
            visibility: hidden;
        }
        #relatorio2{
            visibility: hidden;
        }
        #content{
            padding-top: 20px;
        }
        .panel-header{
            background: #f8f9fa;
            padding: 10px;
            border-top: 1px #ccc solid;
            border-left: 1px #ccc solid;
            border-right: 1px #ccc solid;
        }
        .navbar-brand img{
            width: 150px;
        }
    </style>
    <addMembro></addMembro>
    <criarGrupo></criarGrupo>
    <editarCategoria></editarCategoria>
    <criarTarefa></criarTarefa>
    <cadastrarCategoria></cadastrarCategoria>
    <infoTarefa></infoTarefa>
    <script>
        this.mixin("state")
        this.mixin("reduxGlobal")
        this.mixin('subscribeStateTag')
        this.subscribeStateTag(this)


                gerarGraficoUsers(){
                    document.getElementById('relatorio1').style.visibility = "visible";
                    document.getElementById('relatorio2').style.visibility = "hidden";
                    document.getElementById('pdf1').style.visibility = "visible";
                        var array_user = this.state.dashboardReducer.graph;
                        google.charts.load('current', { packages: ['corechart', 'bar'] });
                        google.charts.setOnLoadCallback(drawBasicc);

                        function drawBasicc() {
                            var data = google.visualization.arrayToDataTable([
                                ['Element', 'Cadastros', { role: 'style' }],
                                [array_user[0].name, array_user[0].count, 'blue'],
                                [array_user[1].name, array_user[1].count, 'blue'],
                                [array_user[2].name, array_user[2].count, 'blue'],
                                [array_user[3].name, array_user[3].count, 'blue'],
                                [array_user[4].name, array_user[4].count, 'blue'],
                                [array_user[5].name, array_user[5].count, 'blue'],
                                [array_user[6].name, array_user[6].count, 'blue'],
                            ]);

                            var chart = new google.visualization.ColumnChart(
                                document.getElementById('chart_div'));

                            chart.draw(data);
                        }
                    }
                    printUsers() {
                        document.getElementById('pdf1').style.visibility = "hidden";
                        document.getElementById('gerar1').style.visibility = "hidden";
                        document.getElementById('pdf2').style.visibility = "hidden";
                        document.getElementById('gerar2').style.visibility = "hidden";
                        window.print();
                        document.getElementById('gerar1').style.visibility = "visible";
                        document.getElementById('gerar2').style.visibility = "visible";
                    }
                                    gerarGraficoGrupos(){
                                            document.getElementById('relatorio1').style.visibility = "hidden";
                                            document.getElementById('relatorio2').style.visibility = "visible";
                                            document.getElementById('pdf2').style.visibility = "visible";
                                            var array_group = this.state.dashboardReducer.graphg;
                                            google.charts.load('current', { packages: ['corechart', 'bar'] });
                                            google.charts.setOnLoadCallback(drawBasic);

                                            function drawBasic() {
                                                var data2 = google.visualization.arrayToDataTable([
                                                    ['Element', 'Cadastros', { role: 'style' }],
                                                    [array_group[0].name, array_group[0].count, 'red'],
                                                    [array_group[1].name, array_group[1].count, 'red'],
                                                    [array_group[2].name, array_group[2].count, 'red'],
                                                    [array_group[3].name, array_group[3].count, 'red'],
                                                    [array_group[4].name, array_group[4].count, 'red'],
                                                    [array_group[5].name, array_group[5].count, 'red'],
                                                    [array_group[6].name, array_group[6].count, 'red'],
                                                ]);

                                                var chart = new google.visualization.ColumnChart(
                                                    document.getElementById('chart_div2'));

                                                chart.draw(data2);
                                            }
                                        }
                                        print() {
                                                document.getElementById('pdf1').style.visibility = "hidden";
                                                document.getElementById('gerar1').style.visibility = "hidden";
                                                document.getElementById('pdf2').style.visibility = "hidden";
                                                document.getElementById('gerar2').style.visibility = "hidden";
                                                window.print();
                                                document.getElementById('gerar1').style.visibility = "visible";
                                                document.getElementById('gerar2').style.visibility = "visible";
                                            }

    </script>
    <header></header>
    <div class="container">
        <button id="gerar1" type="button" class="btn btn-sucess" onclick={gerarGraficoUsers}>Gerar Relatorio De Usuarios</button>
        <button id="pdf1" type="button" class="btn btn-sucess bhide" onclick={printUsers}>Imprimir Relatorio de Usuarios</button>
        <br>
        <br>
        <button id="gerar2" type="button" class="btn btn-sucess" onclick={gerarGraficoGrupos}>Gerar Relatorio De Grupos</button>
        <button id="pdf2" type="button" class="btn btn-sucess bhide" onclick={print}>Imprimir Grafico de Usuarios</button>
        <div id="relatorio1">
            <h1 class="titulo" id="title">BTM - Best Task Manager</h2>
                <br>
                <h2 class="titulo">Relatório de cadastros de usuários</h2>
                <br>
                <br>
                <span class="texto"> O grafico abaixo mostra a quantidade de usuarios que foram cadastrados durante a semana.</span>
                <div id="chart_div"></div>
        </div>
        <div id="relatorio2">
            <h1 class="titulo" id="title">BTM - Best Task Manager</h2>
                <br>
                <h2 class="titulo">Relatório de criação de grupos</h2>
                <br>
                <br>
                <p class="texto"> O grafico abaixo mostra a quantidade de grupos que foram cadastrados durante a semana.</p>
                <div id="chart_div2"></div>
        </div>
    </div>
</anotacoes>
