<notifications>
    <div class="modal fade" id="notifications">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div id="spinner" if={this.state.exibicaoReducer.showSpinner}><img src="imgs/spinner.svg"></div>
                <div class="modal-header">
                    <h4 class="modal-title">Notificações</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
    
                <div class="modal-body">
                    <div class="row-fluid" each={ group_invitation in this.state.dashboardReducer.group_invitations }>
                        <h3>Novo convite para participar do grupo <b>{group_invitation.name}</b></h3>
                        <a class="col-md-5 btn btn-success" onclick={acceptInvite.bind(this,group_invitation.id)}>Aceitar convite</a> <a class="col-md-5 offset-md-1 btn btn-danger" onclick={rejectInvite.bind(this,group_invitation.id)}>Recusar convite</a>
                        <hr>
                    </div>
                </div>  
            </div>
        </div>
    </div>

    <script>
        this.mixin("state")
        this.mixin("reduxGlobal")
        this.mixin('subscribeStateTag')
        this.subscribeStateTag(this)

        acceptInvite(group_id){
            this.dispatch({   
                type: 'ACCEPT_INVITE',
                payload: {
                    token: this.state.userReducer.user_data.user_token,
                    id: group_id
                }
            });
        }

        rejectInvite(group_id){
            this.dispatch({   
                type: 'REJECT_INVITE',
                payload: {
                    token: this.state.userReducer.user_data.user_token,
                    id: group_id
                }
            });
        }

        this.on('update', function() {
            if(this.state.userReducer.closeModal){
                $('#notifications').modal('hide');
            }
        })
    </script>

    <style>
        #spinner{
            position: absolute;
            width: 100%;
            z-index: 10;
            text-align: center;
            height: 100%;
            background: rgba(255, 255, 255, 0.7);
            padding-top: 7%;
        }
    </style>
</notifications>