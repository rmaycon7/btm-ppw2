<editUser>
    <div class="modal fade" id="editUser" tabindex="-1" role="dialog" aria-labelledby="cadastroTitle" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div id="spinner" if={this.state.exibicaoReducer.showSpinner}><img src="imgs/spinner.svg"></div>
                <div class="alert alert-danger alert-dismissible fade show" role="alert" if={this.state.exibicaoReducer.showError}>
                    <strong>Ops!</strong> {this.state.exibicaoReducer.msgError}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-header">
                    <h5 class="modal-title" id="cadastroTitle">Editar</h5>
                </div>
                <div class="modal-body">
                    <form>
                        <p><input autofocus="autofocus" value="{this.state.userReducer.user_data.name}" name="usuario[nome]" id="register_name" type="text" placeholder="Nome" class="form-control" required="required"></p>
                        <p><input autofocus="autofocus" value="{this.state.userReducer.user_data.email}" name="usuario[email]" id="register_email" type="email" placeholder="E-mail" class="form-control" required="required"></p>
                        <p><input autocomplete="off" name="user[password]" id="register_password" type="password" placeholder="Senha" class="form-control" required="required"></p>
                        <p><input autocomplete="off" name="user[password]" id="register_confirmation_password" type="password" placeholder="Confirme sua senha" class="form-control" required="required"></p>
                    </form>
                </div>
                <div class="modal-footer">
                    <input name="commit" value="Atualizar" class="btn btn-success btn-fill btn-block" data-disable-with="Atualizar" type="button" onclick={this.updateUser}>
                </div>
            </div>
        </div>
    </div>

    <script>
        this.mixin("state")
        this.mixin("reduxGlobal")
        this.mixin('subscribeStateTag')
        this.subscribeStateTag(this)

        updateUser(){
            const password = document.getElementById('register_password').value;
            const confirmation_password = document.getElementById('register_confirmation_password').value;
            const user_id = this.state.userReducer.user_data.id;
            const user_token = this.state.userReducer.user_data.user_token;
            if(password == confirmation_password){
                let editUser = {id: user_id, user_token: user_token, name: (document.getElementById('register_name').value), email: (document.getElementById('register_email').value), password: password};
                console.log(editUser);
                this.dispatch({
                    type: "UPDATE_USER",
                    payload: {
                        editUser
                    }
                });
            }else{
                alert('A senha de confirmação está incorreta.');
            }
        }

        this.on('update', function() {
            if(this.state.userReducer.closeModal){
                $('#editUser').modal('hide');
            }
        })
    </script>
    <style>
        #spinner{
            position: absolute;
            width: 100%;
            z-index: 10;
            text-align: center;
            height: 100%;
            background: rgba(255, 255, 255, 0.7);
            padding-top: 7%;
        }
    </style>
</editUser>