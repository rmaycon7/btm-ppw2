import store from "./store";
import Axios from "axios";

//Função que retorna a URL da api.
function urlApi(){
    return 'https://api-dev-api.193b.starter-ca-central-1.openshiftapps.com';
}

//Faz a requisição dos dados do usuario e o redireciona para o dashboard.
export function* login(action) {
    yield put({ type: "SHOW_SPINNER_ON" });
    
    try{   
        let response = "";
        if(response = yield call(Axios.post, urlApi()+"/authenticate", action.payload.newLogin)){ 
            const user_token = response.data.token;
            const id = response.data.id;
            let config = { headers: { 'Authorization': "Bearer " + user_token }};

            const user_data = { user_token: user_token, ...response.data };
            yield put({ type: "SET_USERDATA", payload: user_data });        
            sessionStorage.setItem("btm-user-name", response.data.name);
            sessionStorage.setItem("btm-user-email", response.data.email);
            sessionStorage.setItem("btm-user-token", user_token);
        
            try{
                const groups = yield call(Axios.get, urlApi() + "/groups", config);
                if(groups.data.length > 0){
                    yield put({ type: "SET_CURRENT_GROUP_ID", payload: {id: groups.data[0].id} });
                    yield call(getGroup, { payload: { data: { group_id: groups.data[0].id, token: user_token} } });
                    yield call(getGroupInvitations, { payload: { data: { group_id: groups.data[0].id, token: user_token } } });
                    yield call(graficoUser, user_token)
                    yield call(graficoGroup, user_token)
                }
            }catch(err){
                console.log(err.response)
            }
            yield put({ type: "SHOW_SPINNER_OFF" });
            yield put({ type: "CLOSE_MODAL" });
            window.location.href="#/dashboard";
        }
    }catch(err){
        console.log(err.response);
        //console.log(err.response);
        //console.log(err.response.data);
        yield put({ type: "SHOW_SPINNER_OFF" });
        yield put({ type: 'SHOW_ERROR' });
        yield put({ type: 'MSG_ERROR', payload: "Usuário ou senha incorretos!" });
    }
    
}

export function* logOut(){
    yield put({ type: 'SET_USERDATA', payload: {name: "", email: "", user_token: ""} });
    sessionStorage.setItem("btm-user-name", "");
    sessionStorage.setItem("btm-user-email", "");
    sessionStorage.setItem("btm-user-token", "");
    yield put({ type: "SET_STATE_NULL" });
    window.location.href="/#/home";
}

 
//Registra o usuario, seta os dados e o redireciona para o dashboard.
export function* register(action) {
    yield put({ type: "SHOW_SPINNER_ON" });
    try{
        const response = yield call(Axios.post, urlApi()+"/register", action.payload.newUser);
        const user_data = response.data;
        yield call(graficoUser, user_data.token)
        yield call(graficoGroup, user_data.token)
        yield put({ type: "SET_USERDATA", payload: user_data });
        sessionStorage.setItem("btm-user-name", response.data.name);
        sessionStorage.setItem("btm-user-email", response.data.email);
        sessionStorage.setItem("btm-user-token", response.data.token);

        yield put({ type: "CLOSE_MODAL" });
        yield put({ type: "SHOW_SPINNER_OFF" });
        window.location.href="#/dashboard";
    } catch(err){
        if(err == "Error: Request failed with status code 409"){
            yield put({ type: "SHOW_SPINNER_OFF" });
            yield put({ type: 'SHOW_ERROR' });
            yield put({ type: 'MSG_ERROR', payload: "Este email já está cadastrado!" });
        }
    }
}

//Função para atualizar os dados do usuario.
export function* updateUser(action) {
    yield put({ type: "SHOW_SPINNER_ON" });
    let config = { headers: { 'Authorization': "Bearer " + action.payload.editUser.user_token }};
    try {
        const response = yield call(Axios.patch, urlApi()+"/users/"+action.payload.editUser.id, action.payload.editUser, config);
    }catch(err){
        console.log(err);
        console.log('Erro ao atualizar usuario: '+err.response.data);
    }
    const user_data = { id: "", user_token: "", name: "", email: "" };
    yield put({ type: "SET_USERDATA", payload: user_data });
    yield put({ type: "SHOW_SPINNER_OFF" });
    yield put({ type: "CLOSE_MODAL" });
    yield put({ type: "PROFILE_OFF" });
    yield put({ type: "HOME_ON" });
}

//Atualiza o state com todos os grupos do usuario.
export function* getGroups(action) {
    let config = { headers: { 'Authorization': "Bearer " + action.token }};
    try{
        const response = yield call(Axios.get, urlApi() + "/groups", config);
        const grupos = response.data;
        yield put({ type: "SET_GROUPS", payload: grupos });
    }catch(err){
        console.log(err.response)
    }
}

//Envia um post pra api criar o grupo e em seguida chama a função getGroups que atualiza o state com todos os grupos.
export function* addGroup(action) {  
    yield put({ type: "SHOW_SPINNER_ON" });
    const user_token = action.payload.userinfo.token;
    const user_id = action.payload.userinfo.id;
    let config = { headers: { 'Authorization': "Bearer " + user_token } };
    try{
        const response = yield call(Axios.post, urlApi()+"/groups/", action.payload.new_group, config);
        const attgroups = { id: user_id, token: user_token };
        yield call(getGroups, attgroups);
        yield put({ type: "SET_CURRENT_GROUP_ID", payload: {id: response.data.id} });
        yield put({ type: 'SHOW_ALERT_MSG', payload: "Grupo criado com sucesso!" });
    }catch(err){
        yield put({ type: 'SHOW_ALERT_MSG', payload: "" });
        console.log(err.response);
    }
    yield put({ type: "SHOW_SPINNER_OFF" });
    yield put({ type: "CLOSE_MODAL" });
}

//Envia um patch pra api atualizar o grupo e em seguida chama a função getGroups que atualiza o state com todos os grupos.
export function* editGroup(action) {
    yield put({ type: "SHOW_SPINNER_ON" });
    const user_token = action.payload.userinfo.token;
    const user_id = action.payload.userinfo.id;
    let config = { headers: { 'Authorization': "Bearer " + user_token } };
    try {
        const response = yield call(Axios.patch, urlApi()+'/groups/'+action.payload.groupAtt.id, action.payload.groupAtt, config);
    } catch (error) {
        console.log(error.response)
    }
    const attgroups = { id: user_id, token: user_token };
    
    yield call(getGroups, attgroups);
    yield put({ type: "SHOW_SPINNER_OFF" });
    yield put({ type: "CLOSE_MODAL" });
}

//Envia um delete pra api deletar o grupo e em seguida chama a função getGroups que atualiza o state com todos os grupos.
export function* deleteGroup(action) { 
    const user_token = action.payload.userinfo.token;
    const user_id = action.payload.userinfo.id;
    let config = { headers: { 'Authorization': "Bearer " + user_token } };
    try {
        const response = yield call(Axios.delete, urlApi()+'/groups/'+action.payload.group.id, config);
    } catch (error) {
        console.log(error.response)
    }
    const attgroups = { id: user_id, token: user_token };
    yield call(getGroups, attgroups);
    
}
//Faz um get em todas as categorias de um grupo e insere os dados no state.
export function* getCategories(action){
    let config = { headers: { 'Authorization': "Bearer " + action.payload.data.token } };
    try{
        const response = yield call(Axios.get, urlApi()+"/groups/"+action.payload.data.group_id+"/categories", config);
        const categories = response.data;
        yield put({ type: "SET_GROUP_DATA", payload: { categories: categories, group_id: action.payload.group_id} });
    }catch(err){
        console.log(err.response)
        const categories = [];
        yield put({ type: "SET_GROUP_DATA", payload: categories });
    }
}
//Cria uma categoria e chama a função que atualiza o state de categorias.
export function* addCategory(action) {
    yield put({ type: "SHOW_SPINNER_ON" });
    const user_token = action.payload.userinfo.token;
    const user_id = action.payload.userinfo.id;
    let config = { headers: { 'Authorization': "Bearer " + user_token } };
    try {
        const response = yield call(Axios.post, urlApi()+"/groups/"+action.payload.group.id+"/categories", action.payload.category, config)
    } catch (error) {
        console.log(error)
    }
    const attcategory = { payload: { data: { token: user_token, group_id: action.payload.group.id } } };
    yield call(getGroup, attcategory);
    yield put({ type: "SHOW_SPINNER_OFF" });
    yield put({ type: "CLOSE_MODAL" });
}

//Edita uma categoria e chama a função que atualiza o state de categorias.
export function* editCategory(action) {
    yield put({ type: "SHOW_SPINNER_ON" });
    const user_token = action.payload.userinfo.token;
    const user_id = action.payload.userinfo.id;
    let config = { headers: { 'Authorization': "Bearer " + user_token } };
    try {
        const response = yield call(Axios.patch, urlApi()+"/groups/"+action.payload.group.id+"/categories/"+action.payload.category_att.id, action.payload.category_att, config);
    } catch (error) {
        console.log(error.response)
    }
    const attcategory = { payload: { data: { token: user_token, group_id: action.payload.group.id } } };
    yield call(getGroup, attcategory);
    yield put({ type: "SHOW_SPINNER_OFF" });
    yield put({ type: "CLOSE_MODAL" });
}
//Deleta uma categoria e chama a função que atualiza o state de categorias.
export function* deleteCategory(action) {
    const user_token = action.payload.userinfo.token;
    const user_id = action.payload.userinfo.id;
    let config = { headers: { 'Authorization': "Bearer " + user_token } };
    try {
        const response = yield call(Axios.delete, urlApi()+"/groups/"+action.payload.group.id+"/categories/"+action.payload.category.id, config);
    } catch (error) {
        console.log(error.response)
    }
    const attcategory = { payload: { data: { token: user_token, group_id: action.payload.group.id } } };
    yield call(getGroup, attcategory);
}
//Faz uma requisição pra API solicitando, a quantidade de usuarios cadastrados em cada um dos dias da semana e insere os dados no state.
export function* graficoUser(action) {
    const user_token = action;
    let config = { headers: { 'Authorization': "Bearer " + user_token } };
    try {
        const response = yield call(Axios.get, urlApi()+"/report/users/weekdays", config);
        yield put({ type: "INSERT_GRAPH", payload: response.data });
    } catch (error) {
        console.log(error.response)
    }
}
//Faz uma requisição pra API solicitando, a quantidade de grupos criados em cada um dos dias da semana e insere os dados no state.
export function* graficoGroup(action) {
    const user_token = action;
    let config = { headers: { 'Authorization': "Bearer " + user_token } };
    try {
        const response = yield call(Axios.get, urlApi()+"/report/groups/weekdays", config);
        yield put({ type: "INSERT_GRAPHH", payload: response.data });
    } catch (error) {
        console.log(error.response)
    }
}
//Cria uma nota no grupo atual, e chama a função que atualiza o state.
export function* addNote(action){
    yield put({ type: "SHOW_SPINNER_ON" });
    const user_token = action.payload.userinfo.token;
    let config = { headers: { 'Authorization': "Bearer " + user_token } };
    try{
        const response = yield call(Axios.post, urlApi()+"/groups/"+action.payload.group.id+"/notes", action.payload.note, config);
        const attnotes = { payload: { data: { token: user_token, group_id: action.payload.group.id } } };
        yield call(getGroup, attnotes);
    }catch(err){
        console.log(err.response);
    }
    yield put({ type: "SHOW_SPINNER_OFF" });
    yield put({ type: "CLOSE_MODAL" });
}
//Edita uma nota e chama a função que atualiza o state.
export function* editNote(action) {
    yield put({ type: "SHOW_SPINNER_ON" });
    const user_token = action.payload.userinfo.token;
    let config = { headers: { 'Authorization': "Bearer " + user_token } };
    const note_edited = { name: action.payload.note.name, content: action.payload.note.content };
    try {
        const response = yield call(Axios.patch, urlApi()+"/groups/"+action.payload.group.id+"/notes/"+action.payload.note.id, note_edited, config);
    } catch (error) {
        console.log(error.response)
    }
    const attnotes = { payload: { data: { token: user_token, group_id: action.payload.group.id } } };
    yield call(getGroup, attnotes);
    yield put({ type: "SHOW_SPINNER_OFF" });
    yield put({ type: "CLOSE_MODAL" });
}
//Deleta uma nota e chama a função que atualiza o state.
export function* deleteNote(action) {
    const user_token = action.payload.userinfo.token;
    let config = { headers: { 'Authorization': "Bearer " + user_token } };
    try {
        const response = yield call(Axios.delete, urlApi()+"/groups/"+action.payload.group.id+"/notes/"+action.payload.note.id, config);
    } catch (error) {
        console.log(error.response)
    }
    const attnotes = { payload: { data: { token: user_token, group_id: action.payload.group.id } } };
    yield call(getGroup, attnotes);
}
//Faz a requisição das notas do grupo atual.
export function* getNotes(action) {
    const user_token = action.payload.data.token;
    let config = { headers: { 'Authorization': "Bearer " + user_token } };
    try{
        const response = yield call(Axios.get, urlApi()+"/groups/"+action.payload.data.group_id+"/notes", config);
        yield put({ type: "SET_NOTES", payload: response.data});
    }catch(err){
        console.log(err.response);
    }
}
//Cria uma tarefa em uma categoria do grupo e chama a função para atualizar o state.
export function* addTask(action) {
    yield put({ type: "SHOW_SPINNER_ON" });
    const user_token = action.payload.userinfo.token;
    let config = { headers: { 'Authorization': "Bearer " + user_token } };
    try {
        const response = yield call(Axios.post, urlApi()+ "/groups/" + action.payload.group.id + "/categories/" + action.payload.category.id + "/tasks", action.payload.new_task, config);
    } catch (error) {
        console.log(error.response)
    }
    const att_task = { payload: { data: { token: user_token, group_id: action.payload.group.id }}};
    yield call(getGroup, att_task);
    yield put({ type: "SHOW_SPINNER_OFF" });
    yield put({ type: "CLOSE_MODAL" });
}
//Edita uma tarefa e chama a função para atualizar o state.
export function* editTask(action) {
    yield put({ type: "SHOW_SPINNER_ON" });
    const user_token = action.payload.userinfo.token;
    let config = { headers: { 'Authorization': "Bearer " + user_token } };
    try {
        const response = yield call(Axios.patch, urlApi()+"/groups/"+action.payload.group.id+"/categories/"+action.payload.category.id+"/tasks/"+action.payload.task.id, action.payload.task, config);
    } catch (error) {
        console.log(error.response)
    }
    const att_task = { payload: { data: { token: user_token, group_id: action.payload.group.id } } };
    yield call(getGroup, att_task);
    yield put({ type: "SHOW_SPINNER_OFF" });
    yield put({ type: "CLOSE_MODAL" });
}
//Deleta uma tarefa e chama a função para atualizar o state.
export function* deleteTask(action) {
    const user_token = action.payload.userinfo.token;
    let config = { headers: { 'Authorization': "Bearer " + user_token } };
    try {
        const response = yield call(Axios.delete, urlApi()+ "/groups/" + action.payload.group.id + "/categories/" + action.payload.category.id + "/tasks/" + action.payload.task.id, config);
    } catch (error) {
        console.log(error.response)
    }
    const att_task = { payload: { data: { token: user_token, group_id: action.payload.group.id } } };
    yield call(getGroup, att_task);
}

/* export function* getTasks(action) {
    const user_token = action.payload.userinfo.token;
    let config = { headers: { 'Authorization': "Bearer " + user_token } };

    try {
        const response = yield call(Axios.get, urlApi() + "/groups/" + action.payload.group.id + "/categories/" + action.payload.category.id + "/tasks", config);
        const task = { group_id: action.payload.group.id, category_id: action.payload.category.id,  tasks: response.data };
        console.log(task)
        yield put({ type: "SET_TASKS", payload: task });
        
    } catch (error) {
        console.log(error)
        let task = [];
        yield put({ type: "SET_TASKS", payload: task });
    }
} */

//Envia uma solicitação de entrada no grupo atual para algum usuario do sistema, identificado pelo seu E-MAil.
export function* addMemberToGroup(action){
    yield put({ type: "SHOW_SPINNER_ON" });
    const user_token = action.payload.user_token;
    let config = { headers: { 'Authorization': "Bearer " + user_token } };
    try {
        yield call(Axios.post, urlApi()+"/groups/"+action.payload.group_id+"/members", {email: action.payload.email}, config)
        yield put({ type: 'SHOW_ALERT_MSG', payload: "Convite enviado com sucesso!" });
    } catch (error) {
        yield put({ type: 'SHOW_ALERT_MSG', payload: "" });
        console.log(error.response)
    }
    yield put({ type: "SHOW_SPINNER_OFF" });
    yield put({ type: "CLOSE_MODAL" });
}
//Faz a requisição de convites pendentes do usuario.
export function* getGroupInvitations(action) {
    const user_token = action.payload.data.token;
    let config = { headers: { 'Authorization': "Bearer " + user_token } };
    try{
        const response = yield call(Axios.get, urlApi()+"/users/invitations", config);
        console.log("convites")
        console.log(response.data);
        yield put({ type: "SET_GROUP_INVITATOINS", payload: response.data.invitations});
    }catch(err){
        console.log(err.response);
    }
}
//Faz a requisição de todos os dados do grupo atual.
export function* getGroup(action){
    let config = { headers: { 'Authorization': "Bearer " + action.payload.data.token } };
    try {
        const response = yield call(Axios.get, urlApi() + "/groups/"+action.payload.data.group_id+"/all", config);
        console.log("aqui");
        yield put({ type: "SET_GROUP_DATA", payload: response.data });
    } catch (err) {
        console.log(err.response)
    }
}

export function* acceptInvite(action){
    yield put({ type: "SHOW_SPINNER_ON" });
    let config = { headers: { 'Authorization': "Bearer " + action.payload.token } };
    try {
        const response = yield call(Axios.post, urlApi()+ "/users/invitations/"+action.payload.id, {}, config);
        const groups = yield call(Axios.get, urlApi() + "/groups", config);
        if(groups.data.length > 0){
            yield put({ type: "SET_CURRENT_GROUP_ID", payload: {id: groups.data[0].id} });
            yield call(getGroup, { payload: { data: { group_id: groups.data[0].id, token: action.payload.token} } });
            yield call(getGroupInvitations, { payload: { data: { group_id: groups.data[0].id, token: action.payload.token } } });
            yield call(graficoUser, action.payload.token)
            yield call(graficoGroup, action.payload.token)
        }
        yield put({ type: 'SHOW_ALERT_MSG', payload: "Convite aceito com sucesso!" });
        yield put({ type: "SHOW_SPINNER_OFF" });
        yield put({ type: "CLOSE_MODAL" });
    } catch (error) {
        yield put({ type: 'SHOW_ALERT_MSG', payload: "" });
        console.log(error);
        console.log(error.response)
    }
}

export function* rejectInvite(action){
    yield put({ type: "SHOW_SPINNER_ON" });
    let config = { headers: { 'Authorization': "Bearer " + action.payload.token } };
    try {
        const response = yield call(Axios.post, urlApi()+ "/users/invitations/" + action.payload.id, {}, config);
        const groups = yield call(Axios.get, urlApi() + "/groups", config);
        if(groups.data.length > 0){
            yield put({ type: "SET_CURRENT_GROUP_ID", payload: {id: groups.data[0].id} });
            yield call(getGroup, { payload: { data: { group_id: groups.data[0].id, token: action.payload.token} } });
            yield call(getGroupInvitations, { payload: { data: { group_id: groups.data[0].id, token: action.payload.token } } });
            yield call(graficoUser, action.payload.token)
            yield call(graficoGroup, action.payload.token)
        }
        yield put({ type: 'SHOW_ALERT_MSG', payload: "Convite recusado com sucesso!" });
        yield put({ type: "SHOW_SPINNER_OFF" });
        yield put({ type: "CLOSE_MODAL" });
    } catch (error) {
        yield put({ type: 'SHOW_ALERT_MSG', payload: "" });
        console.log(error.response)
    }
}

export function* getGroupMembers(action) {
    const user_token = action.payload.token;
    let config = { headers: { 'Authorization': "Bearer " + user_token } };
    try{
        const response = yield call(Axios.get, urlApi()+"/groups/"+action.payload.group_id+"/members", config);
        yield put({ type: "SET_GROUP_MEMBERS", payload: response.data.members});
    }catch(err){
        console.log(err.response);
    }
}

export function* leaveGroup(action) {
    yield put({ type: "SHOW_SPINNER_ON" });
    let config = { headers: { 'Authorization': "Bearer " + action.payload.token } };
    try{
        const response = yield call(Axios.delete, urlApi()+"/users/group/"+action.payload.group_id, config);
        console.log('removendo do grupo');
        yield put({ type: 'SHOW_ALERT_MSG', payload: "Removido do grupo com sucesso!" });
        yield put({ type: "CLOSE_MODAL" });
        yield put({ type: "SHOW_SPINNER_OFF" });    
    }catch(err){
        yield put({ type: 'SHOW_ALERT_MSG', payload: "" });
        console.log(err.response);
    }
}

export function* removeMemberGroup(action) {
    yield put({ type: "SHOW_SPINNER_ON" });
    let config = { headers: { 'Authorization': "Bearer " + action.payload.token } };
    try{
        const response = yield call(Axios.delete, urlApi()+"/groups/"+action.payload.group_id+"/members/"+action.payload.member_id, config);
        console.log('removendo do grupo');
        console.log(response.data)
        yield put({ type: 'SHOW_ALERT_MSG', payload: "Removido do grupo com sucesso!" });
    }catch(err){
        yield put({ type: 'SHOW_ALERT_MSG', payload: "" });
        console.log(err.response);
    }
    yield put({ type: "CLOSE_MODAL" });
    yield put({ type: "SHOW_SPINNER_OFF" });
}